# Prometheus Monitoring

- **alert-manager-configuration.yaml:** This file describes an Alertmanager configuration for a Kubernetes cluster, specifying email notifications for high CPU load and pod crash looping alerts, along with email settings such as recipient, sender, and SMTP server.

- **alert-rules.yaml:** This file defines Prometheus alerting rules for a Kubernetes cluster, including alerts for high CPU load on a host and pod crash looping. The rules are defined using PrometheusRule API, with specific expressions, labels, and annotations. The rules are applied to the "monitoring" namespace.

- **redis-rules.yaml:** This YAML file defines a PrometheusRule resource in Kubernetes, which includes two alerts for Redis monitoring: one for when Redis is down and another for when there are too many connections. The alerts include labels and annotations to provide additional information about the alert.

- **redis-values.yaml:** This file is a configuration file for a service monitor, which enables monitoring of a service in a Kubernetes cluster. The serviceMonitor section enables the service monitor and sets its release label, while the redisAddress section sets the address for Redis used by the service being monitored.
